module 0xacab.org/leap/bitmask-vpn

require (
	0xacab.org/leap/go-dialog v0.0.0-20181123042829-0ee8438431a0
	github.com/AllenDang/w32 v0.0.0-20180428130237-ad0a36d80adc
	github.com/ProtonMail/go-autostart v0.0.0-20181114175602-c5272053443a
	github.com/apparentlymart/go-openvpn-mgmt v0.0.0-20161009010951-9a305aecd7f2
	github.com/getlantern/context v0.0.0-20190109183933-c447772a6520
	github.com/getlantern/errors v0.0.0-20180829142810-e24b7f4ff7c7
	github.com/getlantern/golog v0.0.0-20170508214112-cca714f7feb5
	github.com/getlantern/hex v0.0.0-20160523043825-083fba3033ad
	github.com/getlantern/hidden v0.0.0-20160523043807-d52a649ab33a
	github.com/getlantern/ops v0.0.0-20170904182230-37353306c908
	github.com/go-stack/stack v1.8.0
	github.com/golang/text v0.3.0 // indirect
	github.com/gotk3/gotk3 v0.0.0-20190108052711-d09d58ef3476
	github.com/jmshal/go-locale v0.0.0-20161107082030-4f541412d67a
	github.com/mitchellh/go-ps v0.0.0-20170309133038-4fdf99ab2936
	github.com/oxtoacart/bpool v0.0.0-20150712133111-4e1c5567d7c2
	github.com/skratchdot/open-golang v0.0.0-20190104022628-a2dfa6d0dab6
	golang.org/x/text v0.3.0
	golang.org/x/tools v0.0.0-20190206221403-44bcb96178d3 // indirect
)
